#! /usr/bin/env python
"""
Lab 1: Jgraph
Project: Spirograph
COSC594 Advanced Algorithms, Jim Plank, Fall 2021
Author: Aiden Rutter (arutter1)
Description: circle.py
Utilities for rendering circles in jgraph syntax

"""
from math import sqrt
class BezierCircle:
    """

    """

    # optimal value to approximate circular arc
    c = (4/3)*(sqrt(2)-1)

    unit_points = [
        [(0,1),(c,1),(1,c),(1,0)],
        [(1,0),(1,-c),(c,-1),(0,-1)],
        [(0,-1),(-c,-1), (-1, -c),(-1,0)],
        [(-1, 0),(-1,c),(-c,1),(0,1)]
    ]

    def __init__(self, r,x=0,y=0):
      self.radius = r
      # multiply every point on unit circle by r and add offset
      self.curves = [[tuple(map(sum,zip((x,y),map(lambda x: x*r, t)))) for t in arc]
        for arc in BezierCircle.unit_points ]

    def toJGr(self):
        # jgraph reuses endpoints, so pop last
        # make sure to pop

        curves = [[str(x) for t in curve for x in t] for curve in self.curves]
        for i in range(len(curves)-1):
            curves[i].pop()
            curves[i].pop()

        curves = [' '.join(curve) for curve in curves]
        return '\n'.join(curves)

from sys import stderr, argv
if __name__ == "__main__":
    radius = 5
    if len(argv) > 1:
        radius = int(argv[1])

    circle = BezierCircle(radius)
    print(circle.curves, file=stderr)
    print(circle.toJGr())
