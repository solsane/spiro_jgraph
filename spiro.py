#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Lab 1: Jgraph
Project: Spirograph
COSC594 Advanced Algorithms, Jim Plank, Fall 2021
Author: Aiden Rutter (arutter1)
Description: spiro.py
Takes command line args for setup.
"""

import argparse
from math import sin, cos, pi
import os
import json

from circle import BezierCircle

def main():
    # parse required arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--outer-radius', required=True, type=float,
        help="Radius of outer gear. Must be positive.")
    parser.add_argument('--inner-radius', required=True, type=float,
        help="Radius of inner gear. Must be positive and less than outer radius.")
    parser.add_argument('--pinhole-offset', required=True, type=float,
        help="Distance of the pinhole from the center of the inner gear. Should "\
            "be positive and less than radius of inner gear.")

    # parse optional / default arguments
    parser.add_argument('--revolutions', type=int, default=10,
        help="Number of revolutions relative to the inner gear to simulate.")
    parser.add_argument('--theta', type=float, default=.1,
        help="Angular distance (rads) between samples")
    parser.add_argument('--inner-color', type=str, default='black',
        help="Color of the inside ring.")
    parser.add_argument('--outer-color', type=str, default='black',
        help="Color of the outer ring.")
    parser.add_argument('--pinhole-color',type=str, default='red',
        help="Color of the pinhole.")

    color_map = {}
    with open('rainbow.json') as jin:
        color_map = json.loads(jin.read())

    def color_map_to_jgr(s):
        return ' '.join(str(x/255) for x in color_map[s][:-1])

    # TODO: configure number of iterations, resolution
    # TODO: other gear parameters
    # TODO: information belowh
    args = parser.parse_args()

    # validate args
    assert(args.outer_radius > args.inner_radius > args.pinhole_offset > 0)
    assert(args.inner_color in color_map)
    assert(args.outer_color in color_map)
    assert(args.pinhole_color in color_map)

    # initialize vars
    steps = int(args.revolutions * pi / args.theta)
    r1 = args.outer_radius
    r2 = args.inner_radius
    dt = args.theta
    a = args.pinhole_offset
    t = 0

    # print graph info
    print('newgraph')

    # axis
    print('xaxis size {} min {} max {}'.format(2*r1+1, -5*r1, 5*r1))
    print('yaxis size {} min {} max {}'.format(2*r1+1, -5*r1, 5*r1))
    print('nodraw')

    # iterate at given timestep, storing the path
    x = []
    y = []
    for step in range(steps):
        # render outer circle (ring gear) once at origin
        outer_circle = BezierCircle(r1)
        print('newline bezier linethickness 2.0 pts'.format(
            color_map_to_jgr(args.outer_color)))
        print(outer_circle.toJGr())
        if step == 0:
            continue # hack :)

        # render the translated inner circle
        x2 = (r1 - r2)*cos(t)
        y2 = (r1 - r2)*sin(t)
        inner_circle = BezierCircle(r2, x2, y2)

        print('newline bezier color {} linetype dashed pts'.format(
            color_map_to_jgr(args.inner_color)))
        print(inner_circle.toJGr())

        # render the path
        # parametric form of hypotrochoid
        x.append((r1 - r2) * cos(t) + a * cos(((r1-r2)/r2)*t))
        y.append((r1 - r2) * sin(t) - a * sin(((r1-r2)/r2)*t))

        print('newline nobezier color {} pts'.format(
            color_map_to_jgr(args.pinhole_color)))
        print(' '.join([str(x) for t in zip(x,y) for x in t]))

        # render the point
        # TODO: line from center to marker
        print('newcurve marktype circle pts {} {}'.format(x[-1],y[-1]))

        t += dt
        print('newpage copygraph')

if __name__ == "__main__":
    # run as executable
    main()
