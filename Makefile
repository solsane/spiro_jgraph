
# GIFS = out1.gif out2.gif out3.gif out4.gif
GIFS = out1.gif

all: $(GIFS)

%.gif:
	$(./spiro.py --outer_radius=5 --inner_radius=2 --pinhole_offset=1 | jgraph -P | convert -dispose previous - %.gif)

clean:
	rm *.gif
