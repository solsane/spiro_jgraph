# spiro_jgraph

## Usage
Disclaimer: `Makefile` under construction 🙃. To run, use the CLI as described:

`./spiro.py <arguments> | jgraph -P | convert -dispose previous - yourfile.gif`

Some experiments to run: `outer-radius=10`, `inner-radius=5`,`pinhole-offset=1`

12.5, 7.5, 2.5

spiro.py will output jgraph syntax to stdout. It uses argparse.
Expects a relatively modern version of python (>=3.7)
See `./spiro.py --help` for parameters.

Makefile contains a few example configurations. The output is piped to jgraph, which is subsequently piped to convert to be
converted to a gif. Use `make` to run these and output the gifs.

`make view` will attempt to display the gifs.

Be forewarned: conversion to gif may take up to a minute, particularly if you tweak the parameters to increase the resolution. This is not optimized for performance.

No pip dependencies required.

## Motivation
![GIF](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Spiograph_Animation.gif/220px-Spiograph_Animation.gif)

## References
* [Gear terminology](http://www.gizmology.net/gears.htm)
* [Wikipedia](https://en.wikipedia.org/wiki/Spirograph)
* [Bezier Circle](https://spencermortensen.com/articles/bezier-circle/)
* [Jgraph Man Page](https://manpages.debian.org/stretch/jgraph/jgraph.1.en.html)

## Usage In depth
usage: spiro.py [-h] --outer-radius OUTER_RADIUS --inner-radius INNER_RADIUS
                --pinhole-offset PINHOLE_OFFSET [--revolutions REVOLUTIONS]
                [--theta THETA] [--inner-color INNER_COLOR]
                [--outer-color OUTER_COLOR] [--pinhole-color PINHOLE_COLOR]

optional arguments:
  -h, --help            show this help message and exit
  --outer-radius OUTER_RADIUS
                        Radius of outer gear. Must be positive.
  --inner-radius INNER_RADIUS
                        Radius of inner gear. Must be positive and less than
                        outer radius.
  --pinhole-offset PINHOLE_OFFSET
                        Distance of the pinhole from the center of the inner
                        gear. Should be positive and less than radius of inner
                        gear.
  --revolutions REVOLUTIONS
                        Number of revolutions relative to the inner gear to
                        simulate.
  --theta THETA         Angular distance (rads) between samples
  --inner-color INNER_COLOR
                        Color of the inside ring.
  --outer-color OUTER_COLOR
                        Color of the outer ring.
  --pinhole-color PINHOLE_COLOR
                        Color of the pinhole.
